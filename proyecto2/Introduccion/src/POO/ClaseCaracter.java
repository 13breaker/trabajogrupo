package POO;

import java.io.IOException;

public class ClaseCaracter {
	
	public static void main(String[] args) throws IOException {
		char car = 'Z';
		Character caracter = new Character('A');
		System.out.println(caracter);
		System.out.println("Introduzca un caracter: ");
		char  c = (char) System.in.read();	
		System.out.println(c);
		if(Character.isDigit(c)) {
			System.out.println("es un numero");
		}else if(Character.isLetter(c)) {
			System.out.println("Es una letra");
			if(Character.isLowerCase(c)) {
				System.out.println("Es minuscula");
				c = Character.toUpperCase(c);
					System.out.println("Lo hemos convertido en mayuscula " + c);
			}else {
				c = Character.toLowerCase(c);
				System.out.println("Lo hemos convertido a minuscula " + c);
			}
		}
		/*for(int letra = 'a'; letra <= 'z'; letra++)
			System.out.println(letra + " ");
		for(char letra = 'a'; letra <= 'z'; letra++)
			System.out.println(letra + " ");
			*/
	
	}
}
