package AbrahamCruz;

import java.util.Scanner;

public class Ejercicio1
{
	public static void main(String[] args) 
	{
		Scanner entrada = new Scanner(System.in);
		int numSecreto = (int)(Math.random()*100+1);
		int respuesta = 0;
		boolean salir = false;
		do{
			System.out.println("Introduce el numero secreto: ");
			respuesta = entrada.nextInt();
			if(numSecreto<respuesta) {
				System.out.println("Mas bajo");
			}
			else
				System.out.println("Mas alto");
			
		}while(respuesta!=numSecreto && respuesta != -1);
		if(numSecreto == respuesta)
			System.out.println("Correcto");
		else
			System.out.println("Te has rendido");
	}

}
